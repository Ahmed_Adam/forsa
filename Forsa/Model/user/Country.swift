//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Country{
    
    var createdAt : String!
    var id : Int!
    var image : String!
    var name : String!
    var updatedAt : String!
    
    public init?(dictionary: JSON) {
        
        name = dictionary["name"].string
        id = dictionary["id"].int
        image = dictionary["image"].string
        createdAt = dictionary["created_at"].string
        updatedAt = dictionary["updated_at"].string

    }
}

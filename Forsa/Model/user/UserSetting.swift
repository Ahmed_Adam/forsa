//
//  UserSetting.swift
//  Forsa
//
//  Created by Adam on 2/24/18.
//  Copyright © 2018 Adam. All rights reserved.
//


import Foundation
import SwiftyJSON


class UserSetting{
    
    var status : Int!
    var userInformation : UserInformation!
    
    
    public init?(dictionary: JSON) {
        
        status = dictionary["status"].int

        let userinfo = dictionary["user_information"]
        userInformation = UserInformation.init(dictionary: userinfo)
    }
    
}

//
//  UserCategoryData.swift
//  Forsa
//
//  Created by Adam on 1/23/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON


class  UserCategoryData{
    
    var categoryId : String!
    var categoryName : String!
    var companyId : String!
    var companyName : String!
    var createdAt : String!
    var details : String!
    var endDate : AnyObject!
    var endHour : AnyObject!
    var id : Int!
    var images : [String]!
    var startDate : AnyObject!
    var title : String!
    var type : String!
    var updatedAt : String!
    
    public init?(dictionary: JSON) {
        
        categoryId = dictionary["categoryId"].string
        categoryName = dictionary["categoryName"].string
        companyId = dictionary["companyId"].string
        companyName = dictionary["companyName"].string
        details = dictionary["details"].string
        endDate = dictionary["endDate"].arrayObject as AnyObject
        endHour = dictionary["endHour"].arrayObject as AnyObject
        id = dictionary["id"].int
        images = dictionary["images"].arrayObject as! [String]
        startDate = dictionary["startDate"].arrayObject as AnyObject
        title = dictionary["title"].string
        type = dictionary["type"].string
        updatedAt = dictionary["updatedAt"].string
        
    }
    
}

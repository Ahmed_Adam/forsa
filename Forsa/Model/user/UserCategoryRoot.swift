//
//  UserCategory.swift
//  Forsa
//
//  Created by Adam on 1/23/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserCategoryRoot{
    
    var currentPage : Int!
    var data : [UserCategoryData]!
    var firstPageUrl : String!
    var from : Int!
    var lastPage : Int!
    var lastPageUrl : String!
    var nextPageUrl : AnyObject!
    var path : String!
    var perPage : Int!
    var prevPageUrl : AnyObject!
    var to : Int!
    var total : Int!
    
    
    public init?(dictionary: JSON) {
        
        currentPage = dictionary["currentPage"].int
        data = dictionary["data"].arrayObject as! [UserCategoryData]
        firstPageUrl = dictionary["firstPageUrl"].string
        from = dictionary["from"].int
        lastPage = dictionary["lastPage"].int
        lastPageUrl = dictionary["lastPageUrl"].string
        nextPageUrl = dictionary["nextPageUrl"].arrayObject as AnyObject
        path = dictionary["path"].string
        perPage = dictionary["perPage"].int
        prevPageUrl = dictionary["prevPageUrl"].arrayObject as AnyObject
        to = dictionary["to"].int
        total = dictionary["total"].int
        
    }
    
    
}

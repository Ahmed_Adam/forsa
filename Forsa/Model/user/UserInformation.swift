//
//  UserInformation.swift
//  Forsa
//
//  Created by Adam on 2/24/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserInformation{
    
    var cityId : String!
    var countryId : String!
    var createdAt : String!
    var email : String!
    var id : Int!
    var image : String!
    var name : String!
    var phone : String!
    var status : String!
    var uid : AnyObject!
    var updatedAt : String!
    var username : String!
    
    public init?(dictionary: JSON) {
        
        cityId = dictionary["cityId"].string
        countryId = dictionary["countryId"].string
        createdAt = dictionary["createdAt"].string
        email = dictionary["email"].string
        id = dictionary["id"].int
        image = dictionary["image"].string
        name = dictionary["name"].string
        phone = dictionary["phone"].string
        status = dictionary["status"].string
        uid = dictionary["uid"].object as AnyObject
        updatedAt = dictionary["updatedAt"].string
        username = dictionary["username"].string

    }
}

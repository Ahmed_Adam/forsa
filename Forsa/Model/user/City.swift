//    RootClass.swift
//
//    Create by Ahmed Adam on 13/2/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class City{
    
    var countryId : String!
    var createdAt : String!
    var id : Int!
    var name : String!
    var updatedAt : String!
    
    public init?(dictionary: JSON) {
        
        name = dictionary["name"].string
        id = dictionary["id"].int
        countryId = dictionary["country_id"].string
        createdAt = dictionary["created_at"].string
        updatedAt = dictionary["updated_at"].string
        
    }
}

//
//  UserLogIn.swift
//  Forsa
//
//  Created by Adam on 1/20/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON

struct UserLogIn{
    
    var id : Int!
    var status : Int!
    var token : String!
    
    public init?(dictionary: JSON) {
        
        id = dictionary["id"].int
        status = dictionary["status"].int
        token = dictionary["token"].string
       
    }
    
    
}

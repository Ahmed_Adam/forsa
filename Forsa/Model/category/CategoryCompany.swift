//
//  CategoryCompany.swift
//  Forsa
//
//  Created by Adam on 2/24/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON

class CatedoryCompany{
    
    var categoryName : String!
    var companies : Company!
    var status : Int!
    
    
    public init?(dictionary: JSON) {
        
        status = dictionary["status"].int
        categoryName = dictionary["category_name"].string
        let companiesdict = dictionary["companies"]
        companies = Company.init(dictionary: companiesdict)
    }
}

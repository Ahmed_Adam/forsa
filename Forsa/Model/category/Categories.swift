//
//  Categories.swift
//  Forsa
//
//  Created by Adam on 1/23/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON


class Categories{
    
    var createdAt : String!
    var id : Int!
    var image : String!
    var name : String!
    var updatedAt : String!
    
public init?(dictionary: JSON) {
        
        id = dictionary["id"].int
        image = dictionary["image"].string
        createdAt = dictionary["createdAt"].string
        name = dictionary["name"].string
        updatedAt = dictionary["updatedAt"].string
    
    }
    
}

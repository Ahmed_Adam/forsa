//
//    Offer.swift
//
//    Create by Ahmed Adam on 27/1/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class Offer :Codable{
    
    var categoryID : String!
    var categoryName : String!
    var companyId : String!
    var companyName : String!
    var createdAt : String!
    var details : String!
    var endDate : String!
    var endHour : String!
    var id : Int!
    var images : [String]!
    var startDate : String!
    var title : String!
    var type : String!
    var updated_at : String!
    var companyUsername : String!
    var offerTimeout : String!
    var stars : Int!
    var image : String!
    var company_logo : String!
    
    
    public init?(dictionary: JSON) {
        image = dictionary["image"].string
        company_logo = dictionary["company_logo"].string
        categoryID = dictionary["category_id"].string
        categoryName = dictionary["category_name"].string
        companyId = dictionary["company_id"].string
        companyName = dictionary["company_name"].string
        categoryName = dictionary["category_name"].string
        details = dictionary["details"].string
        endDate = dictionary["end_date"].string
        endHour = dictionary["end_hour"].string
        id = dictionary["id"].int
        images = dictionary["images"].arrayObject as? [String]
        startDate = dictionary["start_date"].string
        title = dictionary["title"].string
        type = dictionary["type"].string
        updated_at = dictionary["updated_at"].string
        companyUsername = dictionary["company_username"].string
        offerTimeout = dictionary["offer_timeout"].string
        stars = dictionary["stars"].int
        
    }

    

    
}

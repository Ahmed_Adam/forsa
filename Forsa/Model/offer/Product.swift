//
//    Product.swift
//
//    Create by Ahmed Adam on 22/2/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Product{
    
    var categoryId : String!
    var categoryImage : String!
    var categoryName : String!
    var companyId : String!
    var companyName : String!
    var createdAt : String!
    var details : String!
    var endDate : AnyObject!
    var endHour : AnyObject!
    var id : Int!
    var image : String!
    var onlyDay : String!
    var position : String!
    var special : String!
    var stars : Int!
    var startDate : AnyObject!
    var title : String!
    var type : String!
    var updatedAt : String!
    var visits : String!
    
    public init?(dictionary: JSON) {
        image = dictionary["image"].string
        categoryId = dictionary["category_id"].string
        categoryName = dictionary["category_name"].string
        companyId = dictionary["company_id"].string
        companyName = dictionary["company_name"].string
        categoryName = dictionary["category_name"].string
        details = dictionary["details"].string
        id = dictionary["id"].int
        title = dictionary["title"].string
        type = dictionary["type"].string
        stars = dictionary["stars"].int
        visits = dictionary["visits"].string
    }

}

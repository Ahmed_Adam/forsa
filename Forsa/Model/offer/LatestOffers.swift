import Foundation
import SwiftyJSON

class LatestOffer{
    
    var ads : [AnyObject]!
    var companies : [CompanyProfile]?
    var offers :   CompanyOffer!
    
    
    public init?(dictionary: JSON) {
        
        ads = dictionary["ads"].arrayObject! as [AnyObject]
        companies = dictionary["companies"].arrayValue as? [CompanyProfile]
        offers = dictionary["offers"].dictionaryValue as? CompanyOffer
    }
    
}

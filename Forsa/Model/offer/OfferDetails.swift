//
//    Create by Ahmed Adam on 16/2/2018
//    Copyright © 2018. All rights reserved.


import Foundation
import SwiftyJSON


class OfferDetails{
    
    var companyInformation : CompanyProfile!
    var offerInformation : Offer!
    var status : Int!
    
    
    public init?(dictionary: JSON) {

        let companyInfo = dictionary["company_information"]
        companyInformation = CompanyProfile.init(dictionary : companyInfo )
        
        let offerInfo = dictionary["offer_information"]
        offerInformation = Offer.init(dictionary : offerInfo )
        
        status = dictionary["status"].int
        
    }
    
}

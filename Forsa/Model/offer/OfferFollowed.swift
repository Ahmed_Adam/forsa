//
//  OfferFollowed.swift
//  Forsa
//
//  Created by Adam on 2/21/18.
//  Copyright © 2018 Adam. All rights reserved.


import Foundation
import SwiftyJSON

class OfferFollowed:Codable {
    
    var offers : Offer!
    var status : Int!
    
    
    public init?(dictionary: JSON) {
        
        let offersdict = dictionary["offers"]
        offers = Offer.init(dictionary: offersdict)
        
        status = dictionary["status"].int
        
    }
    
}

//
//  Company.swift
//  Forsa
//
//  Created by Adam on 2/21/18.
//  Copyright © 2018 Adam. All rights reserved.
//
//
//    Company.swift
//
//    Create by Ahmed Adam on 21/2/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Company {
    
    var currentPage : Int!
    var data = [ComapnyData]()
    var firstPageUrl : String!
    var from : Int!
    var lastPage : Int!
    var lastPageUrl : String!
    var nextPageUrl : AnyObject!
    var path : String!
    var perPage : Int!
    var prevPageUrl : AnyObject!
    var to : Int!
    var total : Int!
    
    public init?(dictionary: JSON) {
        

        let companiesfollowed = dictionary["data"].array
        
        for x in companiesfollowed! {
            data.append(ComapnyData.init(dictionary: x)!) 
        }

        currentPage = dictionary["current_page"].int
        firstPageUrl = dictionary["firstPageUrl"].string
        from = dictionary["from"].int
        lastPage = dictionary["lastPage"].int
        nextPageUrl = dictionary["nextPageUrl"].string as AnyObject
        path = dictionary["path"].string
        perPage = dictionary["perPage"].int
        to = dictionary["to"].int
        total = dictionary["total"].int
        
    }
    

}

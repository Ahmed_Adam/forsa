//
//    RootClass.swift
//
//    Create by Ahmed Adam on 7/2/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class CompanyStats{
    
    var disLikes : Int!
    var followers : Int!
    var likes : Int!
    var offersCount : Int!
    var productsCount : Int!
    var visits : String!
    
    public init?(dictionary: JSON) {
        
        disLikes = dictionary["disLikes"].int
        followers = dictionary["followers"].int
        likes = dictionary["likes"].int
        offersCount = dictionary["offers_count"].int
        productsCount = dictionary["products_count"].int
        visits = dictionary["visits"].string

    }
    
}

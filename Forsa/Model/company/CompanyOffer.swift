//
//    Company.swift
//
//    Create by Ahmed Adam on 27/1/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


class CompanyOffer{
    
    var currentPage : Int!
    var data : [Offer]!
    var firstPageUrl : String!
    var from : Int!
    var lastPage : Int!
    var lastPageUrl : String!
    var nextPageUrl : AnyObject!
    var path : String!
    var perPage : Int!
    var prevPageUrl : AnyObject!
    var to : Int!
    var total : Int!
    
    public init?(dictionary: JSON) {
        
        currentPage = dictionary["id"].int
        data = dictionary["data"].arrayObject as! [Offer]
        firstPageUrl = dictionary["firstPageUrl"].string
        from = dictionary["from"].int
        lastPage = dictionary["lastPage"].int
        nextPageUrl = dictionary["nextPageUrl"].string as AnyObject
        path = dictionary["path"].string
        perPage = dictionary["perPage"].int
        to = dictionary["to"].int
        total = dictionary["total"].int
    }
    
    
}

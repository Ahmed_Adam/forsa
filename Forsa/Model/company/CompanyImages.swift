//
//    CompanyImage.swift
//
//    Create by Ahmed Adam on 22/2/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON
 
class CompanyImage{
    
    var companyId : String!
    var createdAt : String!
    var id : Int!
    var image : String!
    var updatedAt : String!
    
    public init?(dictionary: JSON) {
        
        image = dictionary["image"].string
        companyId = dictionary["company_id"].string
        id = dictionary["id"].int
        createdAt = dictionary["created_at"].string
        updatedAt = dictionary["updated_at"].string

    }
}

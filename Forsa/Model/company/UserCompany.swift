//
//    RootClass.swift
//
//    Create by Ahmed Adam on 27/1/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class UserCompany{
    
    var currentPage : Int!
    var data : [CompanyProfile]!
    var firstPageUrl : String!
    var from : Int!
    var lastPage : Int!
    var lastPageUrl : String!
    var nextPageUrl : AnyObject!
    var path : String!
    var perPage : Int!
    var prevPageUrl : AnyObject!
    var to : Int!
    var total : Int!
    
    
    public init?(dictionary: JSON) {
        
        currentPage = dictionary["currentPage"].int
        data = dictionary["data"].arrayObject as! [CompanyProfile]
        firstPageUrl = dictionary["firstPageUrl"].string
        from = dictionary["from"].int
        lastPage = dictionary["lastPage"].int
        lastPageUrl = dictionary["lastPageUrl"].string
        path = dictionary["path"].string
        perPage = dictionary["perPage"].int
        prevPageUrl = dictionary["prevPageUrl"].string as AnyObject
        nextPageUrl = dictionary["nextPageUrl"].string as AnyObject
        to = dictionary["to"].int
        total = dictionary["total"].int


        
    }
    
    
}

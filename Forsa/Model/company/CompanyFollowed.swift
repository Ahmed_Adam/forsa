//
//  CompanyFollowed.swift
//  Forsa
//
//  Created by Adam on 2/21/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import SwiftyJSON

class CompanyFollowed{
    
    var companiesfollowed : Company!
    var status : Int!
    
    
    public init?(dictionary: JSON) {
        
        let companiesdict = dictionary["companies"]
        companiesfollowed = Company.init(dictionary: companiesdict)


        
        status = dictionary["status"].int
        
    }
    
}

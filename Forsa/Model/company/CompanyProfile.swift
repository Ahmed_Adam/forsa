//
//    RootClass.swift
//
//    Create by Ahmed Adam on 26/1/2018
//    Copyright © 2018. All rights reserved.


import Foundation
import SwiftyJSON



// Show company profile


class CompanyProfile:Codable{
    
    
    
    var address : String!
    var cityId : String!
    var countryId : String!
    var createdAt : String!
    var email : String!
    var facebook : String!
    var googleplus : String!
    var id : Int!
    var instagram : String!
    var lat : String!
    var logo : String!
    var lon : String!
    var name : String!
    var phone : String!
    var snapchat : String!
    var status : String!
    var twitter : String!
    var updatedAt : String!
    var username : String!
    var visits : String!
    var website : String!
    var whatsapp : String!
    var youtube : String!
    
    
    public init?(dictionary: JSON) {
        
        address = dictionary["address"].string
        cityId = dictionary["cityId"].string
        countryId = dictionary["countryId"].string
        createdAt = dictionary["createdAt"].string
        email = dictionary["email"].string
        facebook = dictionary["facebook"].string
        googleplus = dictionary["googleplus"].string
        id = dictionary["id"].int
        instagram = dictionary["instagram"].string
        lat = dictionary["lat"].string
        logo = dictionary["logo"].string
        lon = dictionary["lon"].string
        name = dictionary["name"].string
        phone = dictionary["phone"].string
        snapchat = dictionary["snapchat"].string
        status = dictionary["status"].string
        twitter = dictionary["twitter"].string
        updatedAt = dictionary["updatedAt"].string
        username = dictionary["username"].string
        visits = dictionary["visits"].string
        website = dictionary["website"].string
        whatsapp = dictionary["whatsapp"].string
        youtube = dictionary["youtube"].string
        
    }
    
    
}

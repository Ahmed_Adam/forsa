//
//    RootClass.swift
//
//    Create by Ahmed Adam on 22/2/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class CompanyDetails{
    
    var companyImages = [CompanyImage]()
    var companyInformation : CompanyProfile?
    var offers = [Offer]()
    var products = [Product]()
    var status : Int!
    
    
    public init?(dictionary: JSON) {
        
        let companyprof = dictionary["company_information"]
        companyInformation = CompanyProfile.init(dictionary: companyprof)
        
        
        let images = dictionary["company_images"].array
        
        for x in images! {
            companyImages.append(CompanyImage.init(dictionary: x)!)
        }
        let offersArr = dictionary["offers"].array
        
        for x in offersArr! {
            offers.append(Offer.init(dictionary: x)!)
        }
        
        let productsArr = dictionary["products"].array
        
        for x in productsArr! {
            products.append(Product.init(dictionary: x)!)
        }

        status = dictionary["status"].int

        
    }
    
}

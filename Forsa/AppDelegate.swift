//
//  AppDelegate.swift
//  Forsa
//
//  Created by Adam on 1/2/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        
        Fabric.with([Crashlytics.self])
        return true
    }




}


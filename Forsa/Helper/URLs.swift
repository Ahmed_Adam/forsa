//
//  URLs.swift
//  Forsa
//
//  Created by Adam on 1/10/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation

struct URLs {
    

    //   User
    static let login = "http://204.93.167.45/~forsa/api/user-login"
    static let register = "http://204.93.167.45/~forsa/api/user-register"
    
    static let resetPassword = "http://204.93.167.45/~forsa/api/company-reset-password"
    
    static let companyLogin = "http://204.93.167.45/~forsa/api/company-login"
    static let companyRegister = "http://204.93.167.45/~forsa/api/company-register"
    
    //Offer
    
      static let storeOffer = "http://204.93.167.45/~forsa/api/company/store-offer"
      static let editOffer = "http://204.93.167.45/~forsa/api/company/edit-offer"
      static let updateOffer = "http://204.93.167.45/~forsa/api/company/update-offer"
    
     static let categories = "http://204.93.167.45/~forsa/api/categories"
     static let user_categories = "http://204.93.167.45/~forsa/api/user/categories"
     static let specificCategory = "http://204.93.167.45/~forsa/api/user/category/"
    
    
    static let companies = "http://204.93.167.45/~forsa/api/user/companies"
    static let specificCompany = "http://204.93.167.45/~forsa/api/user/company"
    
    
    static let latestOffers = "http://204.93.167.45/~forsa/api/user/latest-offers"
    static let follow = "http://204.93.167.45/~forsa/api/user/follow"
    static let like = "http://204.93.167.45/~forsa/api/user/like"
    static let rank = "http://204.93.167.45/~forsa/api/user/rank"
    
   
}

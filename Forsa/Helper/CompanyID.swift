//
//  CompanyID.swift
//  Forsa
//
//  Created by Adam on 2/7/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
class COMPANYID: NSObject {
    
    class func saveCompanyID (id:Int){
        let def = UserDefaults.standard
        def.setValue(id , forKey: "CompanyID")
        def.synchronize()
    }
    class  func getCompanyID ()->Int? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "CompanyID") as? Int)
    }
    
}

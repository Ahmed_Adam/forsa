//
//  Token.swift
//  Forsa
//
//  Created by Adam on 1/12/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
class Token: NSObject {
    
    class func saveAPIToken (token:String){
        let def = UserDefaults.standard
        def.setValue(token , forKey: "api_token")
        def.synchronize()
    }
    class  func getAPIToken ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "api_token") as? String)
    }
    
}

//
//  CountryID.swift
//  Forsa
//
//  Created by Adam on 2/7/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
class CountryID: NSObject {
    
    class func saveCountryID (id:Int){
        let def = UserDefaults.standard
        def.setValue(id , forKey: "CountryID")
        def.synchronize()
    }
    class  func getCountryID ()->Int? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "CountryID") as? Int)
    }
    
}

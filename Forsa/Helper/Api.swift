//
//  Api.swift
//  Forsa
//
//  Created by Adam on 1/12/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class Api {
    static let sharedInstance = Api()
    
    
    func login (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool , _ status:Int) -> Void){
        
        
        let url = URLs.login
        let parameters = [
                            "username"  : info["username" ],
                            "password"  : info["password" ]
                         ]
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success(let value ):
                print("JSON: \(value)")
                let json = JSON(value)
                let user = json["status"].int
                if let token  =  json["token"].string {
                    Token.saveAPIToken(token: token)
                    complition(nil, true , user!)
                }
                    if user == 0 {
                        complition(nil, true , user!)
                    }

            case .failure (let error):
                print(error)
                complition(nil, true , 0)
                
            }
        }

    }
    
    
    func register  (info: [String : Any ] , complition :   @escaping (_ error:Error? ,_ success: Bool, _ isRegistered:Bool) -> Void){
        
        
        let url = URLs.register
        let parameters = [
            "username"  : info["username" ],
            "password"  : info["password" ],
            "phone"     : info["phone" ],
            "email"  : info["email" ],
            "name"  : info["name" ],
            "image"  : info["image" ],
            "country_id"  : info["country_id" ],
            "city_id"  : info["city_id" ],
        ]
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success(let value ):
                print("JSON: \(value)")
                let json = JSON(value)
                if json["model"]["id"].string != nil {
                //    USER_MAXID.save_User_ID(user_id: "\(id)")
                    complition(nil, true, true)
                }
                
                if json["msg"].string == "User not verified" {
                    
                    complition(nil, true, false)
                }
                
                print ("hi after datajsonArray  array" )
                
            case .failure (let error):
                print(error)
                
            }
        }
        
        
    }
    func companyLogin (info: [String :String ] , complition :   @escaping (_ error:Error? ,_ success: Bool, _ isRegistered:Bool) -> Void){
        
        
        let url = URLs.companyLogin
        let parameters = [
            "username"  : info["username" ],
            "password"  : info["password" ]
        ]
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success(let value ):
                print("JSON: \(value)")
                let json = JSON(value)
                if json["model"]["id"].string != nil {
              //      USER_ID.save_User_ID(user_id: "\(id)")
                    complition(nil, true, true)
                }
                
                if json["msg"].string == "User not verified" {
                    
                    complition(nil, true, false)
                }
                
                print ("hi after datajsonArray  array" )
                
            case .failure (let error):
                print(error)
                
            }
        }
        
    }
    
    
    func companyRegister  (info: [String : Any ] , complition :   @escaping (_ error:Error? ,_ success: Bool, _ isRegistered:Bool) -> Void){
        
        
        let url = URLs.companyRegister
        let parameters = [
            "name"      :info["name"       ],
            "phone"     :info["phone"      ],
            "whatsapp"  :info["whatsapp"   ],
            "logo"      :info["logo"       ],
            "email"     :info["email"      ],
            "website"   :info["website"    ],
            "username"  :info["username"   ],
            "password"  :info["password"   ],
            "country_id":info["country_id" ],
            "city_id"   :info["city_id"    ],
            "address"   :info["address"    ],
            "lat"       :info["lat"        ],
            "lon"       :info["lon"        ],
            "facebook"  :info["facebook"   ],
            "twitter"   :info["twitter"    ],
            "youtube"   :info["youtube"    ],
            "googleplus":info["googleplus" ],
            "instagram" :info["instagram"  ],
            "snapchat"  :info["snapchat"   ]
            ]
        let urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        
        Alamofire.request(request).responseJSON { (response) in
            
            switch response.result {
            case .success(let value ):
                print("JSON: \(value)")
                let json = JSON(value)
                if let id  =  json["model"]["id"].string {
                   // USER_ID.save_User_ID(user_id: "\(id)")
                    complition(nil, true, true)
                }
                
                if json["msg"].string == "User not verified" {
                    
                    complition(nil, true, false)
                }
                
                print ("hi after datajsonArray  array" )
                
            case .failure (let error):
                print(error)
                
            }
        }
        
        
    }
    
    
    func GetCompanies ( complition :  @escaping (_ error:Error? ,_ success: Bool ,_ allCompanies :[CompanyProfile]?)->Void){

        
        
        let url = URLs.companies
        var compnyArr = [CompanyProfile]()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success(let value):
                print (value)
                
                let json = JSON(value)
                let dataJesonArray  =  json.dictionary!["data"]?.array
                print ("hi after datajsonArray  array" )
                for data in dataJesonArray! {
                    let company = CompanyProfile(dictionary: json)
                    company?.name = data["name"].string
                    company?.logo = data["logo"].string

                    compnyArr.append(company!)
                    print ("HI from webservice ")
                }
                complition(nil, true, compnyArr)
                
                
            }
        }
    }
    
    
    
     func GetCategories ( complition :  @escaping (_ error:Error? ,_ success: Bool ,_ allCat :[Categories]?)->Void){
        
        
//         categoryID   1?user_id=12&
//        token=3berlUwVgbPzW3a20trEO2cHAXe2aJAlfOQb1xLTvpZuf73FYOFmNpwbLyxuIEVP&
//        page=1
        
        
        let url = URLs.categories 
        var catArr = [Categories]()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success(let value):
                print (value)
                
                let json = JSON(value)
                let dataJesonArray  =  json.array
                print ("hi after datajsonArray  array" )
                for data in dataJesonArray! {
//                    guard let data = data.dictionary else {return}
                    let cat = Categories(dictionary: json)
                    cat?.createdAt = data["createdAt"].string
                    cat?.id = data["id"].int
                    cat?.image = data["image"].string
                    cat?.name = data["name"].string
                    cat?.updatedAt = data["updatedAt"].string
                    catArr.append(cat!)
                    print ("HI from webservice ")
                }
                complition(nil, true, catArr)
                
                
            }
        }
    }
    
    
    func GetLatestOffers ( complition :  @escaping (_ error:Error? ,_ success: Bool ,_ allCompanies :[LatestOffer]?)->Void){
        
        
        
        let url = URLs.latestOffers + "/1"
        var latestOfferArr = [LatestOffer]()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success(let value):
                print (value)
                
                let json = JSON(value)
                let dataJesonArray  =  json.dictionary!["data"]?.dictionary
                print ("hi after datajsonArray  array" )
     
                    let offer = LatestOffer(dictionary: json)
                offer?.ads = dataJesonArray!["ads"]?.arrayObject! as! [AnyObject]
//                offer?.companies = dataJesonArray!["companies"]
//                offer?.offers  = dataJesonArray!["offers"]?.dictionary
                    
                        latestOfferArr.append(offer!)
                    print ("HI from webservice ")
                
                complition(nil, true, latestOfferArr)
                
                
            }
        }
    }
    
    
    
    
}




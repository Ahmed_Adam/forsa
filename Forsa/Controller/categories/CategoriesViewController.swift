//
//  CategoriesViewController.swift
//  Forsa
//
//  Created by Adam on 1/28/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource {

    var allCat = [Categories]()
    @IBOutlet weak var collectionview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionview.delegate = self
        collectionview.dataSource = self
        getCat()
    }
    
    
    func getCat() {
        Api.sharedInstance.GetCategories { (error, success, allCat) in
            
            self.allCat = allCat!
            
            self.collectionview.reloadData()
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCat.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CategoriesCompaniesViewController") as! CategoriesCompaniesViewController
         vc.cat_id = allCat[indexPath.row].id
        self.show(vc, sender: self)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CateforiesCollectionViewCell
        
        let arrayOf_dataInJSON = allCat[indexPath.row]
        cell.name.text = arrayOf_dataInJSON.name
        
        Alamofire.request( arrayOf_dataInJSON.image ).responseData { (response ) in
            
            cell.logo.image = UIImage(data: response.data!)
        }
        
        
        return cell
    }


}

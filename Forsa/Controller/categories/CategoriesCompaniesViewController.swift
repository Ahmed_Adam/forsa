//
//  CategoriesCompaniesViewController.swift
//  Forsa
//
//  Created by Adam on 2/24/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesCompaniesViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    var allCompanies : CatedoryCompany!
    var companiesdata = [ComapnyData]()
    var cat_id :Int!
    var followcompany = "2"
     var company_id : Int!
    
    
    @IBOutlet weak var tableview: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return companiesdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! companyFollowedTableViewCell
        
        let companyInfo = self.companiesdata[indexPath.row]
        cell.companyName.text = companyInfo.name
        company_id = Int(companyInfo.id)
        
        cell.followButton.tag = indexPath.row
        cell.followButton.superview?.tag = indexPath.section
        cell.followButton.addTarget(self, action: #selector(follow(_:)), for: .touchUpInside)

        Alamofire.request( companyInfo.logo ).responseData { (response ) in
            
            cell.companylogo.image = UIImage(data: response.data!)
        }
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCompany()

        tableview.delegate = self
        tableview.dataSource = self
    }

    func getCompany() {
        
        Api.sharedInstance.GetCategoryCompanies(cat_id: "\(cat_id!)") { (error, success, cat_companies) in
  
            self.allCompanies = cat_companies
            self.companiesdata = (cat_companies?.companies.data)!
            self.tableview.reloadData()
        }
        
    }
    
    
    @objc func follow(_ sender: UIButton) {
        
        
        let user_id = USERID.getUserID()
        if user_id == 0 {
            let alert = UIAlertController(title: "Alert", message: "you should loin first  ", preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                    self.navigationController?.pushViewController(linkingVC, animated: true)
                    
                    
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }}))
        }
        else{
            
            if followcompany == "2" {
                
                let info = [
                    "company_id"  : company_id ,
                    "type"  : 1
                ]
                Api.sharedInstance.follow(info: info) { (error, success, status) in
                    
                    if status == 1{
                        
                        sender.setTitle("متابع", for: .normal )
                        self.followcompany = "1"
                        self.getCompany()
                    }
                    else   if status == 0{
                        
                        sender.setTitle("تابع", for: .normal )
                        self.followcompany = "2"
                        self.getCompany()
                    }
                        
                    else
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: "you shoud loin first  ", preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                                self.navigationController?.pushViewController(linkingVC, animated: true)
                                
                                
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        
                        alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }}))
                    }
                }
            }
                
            else if followcompany == "1" {
                let info = [
                    "company_id"  : company_id ,
                    "type"  : 2
                ]
                
                Api.sharedInstance.follow(info: info) { (error, success, status) in
                    
                    if status == 0{
                        
                        sender.setTitle("تابع", for: .normal )
                        self.followcompany = "2"
                    }
                    else  if status == 1{
                        
                        sender.setTitle("متابع", for: .normal )
                        self.followcompany = "1"
                    }
                    else
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: "you shoud loin first  ", preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                                self.navigationController?.pushViewController(linkingVC, animated: true)
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }}))
                        alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }}))
                        
                        
                    }
                    
                }
                
            }
        }
        
    }

}

//
//  ViewController.swift
//  Forsa
//
//  Created by Adam on 1/2/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Alamofire

class ForsaViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UICollectionViewDelegate , UICollectionViewDataSource {

    
    var allCat = [Categories]()
    var allCompanies = [CompanyProfile]()
    var offer_id : Int!
    var company_id : Int!
    
    @IBOutlet weak var companiesCollection: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   allCompanies.count
    }
    

    
    var x = true
    
    
    @objc func like(_ sender: AnyObject) {
        
        let info = [
            "offer_id"  : offer_id! ,
            "type"  : 1
        ]
        Api.sharedInstance.Like(info: info) { (error, success, response) in
          
            if response == 1   {
                let image = #imageLiteral(resourceName: "Like_icon")
                sender.setImage(image, for: .normal)
  
            }
            else if response == 0
            {
                let image = #imageLiteral(resourceName: "Like_icon-1")
                sender.setImage(image, for: .normal)
                let alert = UIAlertController(title: "Alert", message: "you should loin first  ", preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                        self.navigationController?.pushViewController(linkingVC, animated: true)

                        
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))

            }
            else {
                let image = #imageLiteral(resourceName: "Like_icon-1")
                sender.setImage(image, for: .normal)
            }
            
            
        }
    
        
    }
    
    @objc func dislike(_ sender: AnyObject) {
        let info = [
            "offer_id"  : offer_id!,
            "type"  : 2
        ]
        
        Api.sharedInstance.Like(info: info) { (error, success, response) in
            
          if response == 1{
        let image = #imageLiteral(resourceName: "unLike_icon")
        sender.setImage(image, for: .normal)
            self.x = true
            
        }
        else if response == 0
        {
            let image = #imageLiteral(resourceName: "unLike_icon-1")
            sender.setImage(image, for: .normal)
            let alert = UIAlertController(title: "Alert", message: "you shoud loin first  ", preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                    self.navigationController?.pushViewController(linkingVC, animated: true)
                    
                    
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }}))

           
        }
            else {
                let image = #imageLiteral(resourceName: "unLike_icon-1")
            sender.setImage(image, for: .normal)
            }
      }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let Company = self.storyboard?.instantiateViewController(withIdentifier: "CompanyProfileViewController") as! CompanyProfileViewController
        Company.company_id = company_id!
        self.show(Company, sender: self)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = companiesCollection.dequeueReusableCell(withReuseIdentifier: "Companycell", for: indexPath) as! CompaniesCollectionViewCell
        
        let arrayOf_dataInJSON = allCompanies[indexPath.row]
     
        company_id = Int(arrayOf_dataInJSON.id)
        
        Alamofire.request( arrayOf_dataInJSON.logo ).responseData { (response ) in
            
            cell.companyButton.setImage(UIImage(data: response.data!), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offerArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        let offerDetails = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailsViewController") as! OfferDetailsViewController
        offerDetails.id = offer_id!
        
        self.show(offerDetails, sender: self)
       
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ForsaHomeCategoriesTableViewCell
     
     //    var company = self.offercompanies[indexPath.row]
        let offer = self.offerArr[indexPath.row]
        let rate = offer.stars
        

        
        
        
        cell.companyName.text = offer.companyName
        cell.offerState.text = "متبقي من العرض " + offer.offerTimeout + " و " + offer.endHour
        cell.offerdetailstext.text = offer.details
        cell.companyName.text = offer.companyName
        
        cell.likeButton.tag = indexPath.row
        cell.likeButton.superview?.tag = indexPath.section
        cell.likeButton.addTarget(self, action: #selector(like(_:)), for: .touchUpInside)
         offer_id = offer.id
    //    company_id = Int(offer.companyId)
        
        cell.disLikeButton.tag = indexPath.row
        cell.disLikeButton.superview?.tag = indexPath.section
        cell.disLikeButton.addTarget(self, action: #selector(dislike(_:)), for: .touchUpInside)
        
    let image = offer.image
        let logo = offer.company_logo
        Alamofire.request( logo!).responseData { (response ) in
            
            cell.companyLogoButton.setImage(UIImage(data: response.data!), for: .normal)
            
            
        }

        Alamofire.request( image! ).responseData { (response ) in
            
            cell.offerImage.image = UIImage(data: response.data!)
        }
       
      
        
      
        return cell
    }
    
    func starRatingViewValueChange() {
//        valueLab.text = "Value : \(starRatingView.value)"
    }
    
    func starRatingViewSpacingChange() {
//        spacingLab.text = "Spacing : \(starRatingView.spacing)"
    }

    
    @objc func add2cart(_ sender:  Any) {
            starRatingViewValueChange()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCompany()
        getLatestOffers()

        tableview.delegate = self
        tableview.dataSource = self
        companiesCollection.delegate = self
        companiesCollection.dataSource = self
        

    }

    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var asd: UIButton!
    
    var ads : [AnyObject]!
    var offercompanies = [CompanyProfile]()
    var offerArr = [Offer]()
    var Latestoffers :   [String : Any] = [:]
    var offer : Any!
    
    func getLatestOffers() {
        Api.sharedInstance.GetLatestOffers { (error, success, companies, offer , offersArr )  in
            

            self.offercompanies = companies!
            self.Latestoffers = offer as! [String : Any]
            let Companyoffers = self.Latestoffers["data"]
            self.offer = Companyoffers
            self.offerArr = offersArr
            self.tableview.reloadData()
        }
            
        }
        


    
    func getCompany() {
        Api.sharedInstance.GetCompanies { (error, success, allCompanies) in
            self.allCompanies = allCompanies!
            
            self.companiesCollection.reloadData()
        }
    }
  



}


//
//  CompanyFollowViewController.swift
//  Forsa
//
//  Created by Adam on 1/28/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Alamofire

class CompanyFollowViewController: UIViewController, UITableViewDelegate , UITableViewDataSource, UICollectionViewDelegate , UICollectionViewDataSource{

    var allCompanies = [CompanyProfile]()
    var companiesdata = [ComapnyData]()
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var companiesCollection: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   allCompanies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = companiesCollection.dequeueReusableCell(withReuseIdentifier: "Companycell", for: indexPath) as! CompaniesCollectionViewCell
        let arrayOf_dataInJSON = allCompanies[indexPath.row]
        //  cell.companyButton.setTitle( arrayOf_dataInJSON.name, for: .normal)
        
        
        Alamofire.request( arrayOf_dataInJSON.logo ).responseData { (response ) in
            
            cell.companyButton.setImage(UIImage(data: response.data!), for: .normal)
        }
        return cell
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return companiesdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! companyFollowedTableViewCell
        
        let companyInfo = self.companiesdata[indexPath.row]
        cell.companyName.text = companyInfo.name
        
        cell.followButton.setTitle("متابع", for: .normal)
        Alamofire.request( companyInfo.logo ).responseData { (response ) in
            
            cell.companylogo.image = UIImage(data: response.data!)
        }
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user_id = USERID.getUserID()
        if user_id == 0 {
            let alert = UIAlertController(title: "Alert", message: "you should loin first  ", preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                    self.navigationController?.pushViewController(linkingVC, animated: true)
                    
                    
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }}))
        }
        else{
  
        getCompany()
        getFollowedCompany()
        tableview.delegate = self
        tableview.dataSource = self
        companiesCollection.delegate = self
        companiesCollection.dataSource = self
        }
    }
    
    func getCompany() {
        Api.sharedInstance.GetCompanies { (error, success, allCompanies) in
            self.allCompanies = allCompanies!
            
            self.companiesCollection.reloadData()
        }
    }
    
    func getFollowedCompany(){
        Api.sharedInstance.GetFollowedCompanies { (error, success, compaiesFollowedArr) in
            
            self.companiesdata = (compaiesFollowedArr?.companiesfollowed.data)!

            self.tableview.reloadData()
           // print(compaiesFollowedArr?.companies?.address!)
        }
    }
    
    
}


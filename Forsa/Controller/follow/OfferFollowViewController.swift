//
//  FollowViewController.swift
//  Forsa
//
//  Created by Adam on 1/28/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Alamofire

class OfferFollowViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  , UICollectionViewDelegate , UICollectionViewDataSource{

    var allCompanies = [CompanyProfile]()
    @IBOutlet weak var tableview: UITableView!
    
    
    var ads : [AnyObject]!
    var offercompanies = [CompanyProfile]()
    var offerArr = [Offer]()
    var Latestoffers :   [String : Any] = [:]
    var offer : Any!
    
    @IBOutlet weak var companiesCollection: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   allCompanies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = companiesCollection.dequeueReusableCell(withReuseIdentifier: "Companycell", for: indexPath) as! CompaniesCollectionViewCell
        let arrayOf_dataInJSON = allCompanies[indexPath.row]
        //  cell.companyButton.setTitle( arrayOf_dataInJSON.name, for: .normal)
        
        
        Alamofire.request( arrayOf_dataInJSON.logo ).responseData { (response ) in
            
            cell.companyButton.setImage(UIImage(data: response.data!), for: .normal)
        }
        return cell
    }
    
    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user_id = USERID.getUserID()
        if user_id == 0 {
            let alert = UIAlertController(title: "Alert", message: "you should loin first  ", preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                    self.navigationController?.pushViewController(linkingVC, animated: true)
                    
                    
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }}))
        }
        else{
        
        getCompany()
        getLatestOffers()
        tableview.delegate = self
        tableview.dataSource = self
        companiesCollection.delegate = self
        companiesCollection.dataSource = self
        }

    }
    
    func getCompany() {
        Api.sharedInstance.GetCompanies { (error, success, allCompanies) in
            self.allCompanies = allCompanies!
            
            self.companiesCollection.reloadData()
        }
    }

    
    var x = true
    @objc func like(_ sender: AnyObject) {
        
        if x == true{
            let image = #imageLiteral(resourceName: "Like_icon")
            sender.setImage(image, for: .normal)
            x = false
        }
        else
        {
            let image = #imageLiteral(resourceName: "Like_icon-1")
            sender.setImage(image, for: .normal)
            x = true
        }
        
    }
    
    @objc func dislike(_ sender: AnyObject) {
        
        if x == true{
            let image = #imageLiteral(resourceName: "unLike_icon")
            sender.setImage(image, for: .normal)
            x = false
        }
        else
        {
            let image = #imageLiteral(resourceName: "unLike_icon-1")
            sender.setImage(image, for: .normal)
            x = true
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offerArr.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ForsaHomeCategoriesTableViewCell
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ForsaHomeCategoriesTableViewCell
        
       // let company = self.offercompanies[indexPath.row]
        let offer = self.offerArr[indexPath.row]
        
        cell.companyName.text = offer.companyName
//        cell.offerState.text = "متبقي من العرض " + offer.offerTimeout + " و " + offer.endHour
        cell.offerdetailstext.text = offer.details
        cell.companyName.text = offer.companyName
        
        cell.likeButton.tag = indexPath.row
        cell.likeButton.superview?.tag = indexPath.section
        cell.likeButton.addTarget(self, action: #selector(like(_:)), for: .touchUpInside)
        
        
        cell.disLikeButton.tag = indexPath.row
        cell.disLikeButton.superview?.tag = indexPath.section
        cell.disLikeButton.addTarget(self, action: #selector(dislike(_:)), for: .touchUpInside)
        
        let image = offer.image
        let logo = offer.company_logo
        Alamofire.request( logo!).responseData { (response ) in
            
            cell.companyLogoButton.setImage(UIImage(data: response.data!), for: .normal)
            
            
        }
        
        Alamofire.request( image! ).responseData { (response ) in
            
            cell.offerImage.image = UIImage(data: response.data!)
        }
   
        return cell
    }

    func getLatestOffers() {
        Api.sharedInstance.GetLatestOffers { (error, success, companies, offer , offersArr )  in
            
            
            self.offercompanies = companies!
            self.Latestoffers = offer as! [String : Any]
            let Companyoffers = self.Latestoffers["data"]
            self.offer = Companyoffers
            self.offerArr = offersArr
            self.tableview.reloadData()
        }
        
    }
    
    


}


    
    


    
    
    


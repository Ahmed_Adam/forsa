//
//  OfferDetailsViewController.swift
//  Forsa
//
//  Created by Adam on 1/24/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyStarRatingView

class CompanyProfileViewController: UIViewController {

//    @IBAction func ttt(_ sender: SwiftyStarRatingView) {
//    }
//    @IBOutlet weak var s: SwiftyStarRatingView!
    var company_id :Int!
    @IBOutlet weak var starRatingView: SwiftyStarRatingView!

    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var value: UILabel!
    @IBAction func starRatingValueChanged(_ sender: SwiftyStarRatingView) {
        starRatingView.value = CGFloat(sender.value)
        value.text = "\(starRatingView.value)"
    }
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var CompanyLogo: UIImageView!
    @IBOutlet weak var companyPhone: UIButton!
    @IBOutlet weak var companyEmail: UIButton!
    @IBOutlet weak var companyFB: UIButton!
    @IBOutlet weak var CompanyInsta: UIButton!
    @IBOutlet weak var companyTwitter: UIButton!
    @IBOutlet weak var companyYoutube: UIButton!
    @IBOutlet weak var companySnapchat: UIButton!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch segment.selectedSegmentIndex {
        case 0 : break
            
        case 1 : break
           
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          starRatingView.allowsHalfStars = false
        getCompanyDetails()
    }

    func getCompanyDetails(){
        
        Api.sharedInstance.GetCompanyDetails(company_id: company_id) { (error, success, companyInfo ) in
            
            
            self.companyName.text = companyInfo?.companyInformation?.name
            if companyInfo?.companyInformation?.status == "1" {
                self.followButton.setTitle("متابع", for: .normal)
            }
            else {
                self.followButton.setTitle("تابع", for: .normal)
            }
            var image = companyInfo?.companyImages[0].image
            Alamofire.request( image!).responseData { (response ) in
                
                self.productImage.image = UIImage(data : response.data!)
                
                
            }
            image = companyInfo?.companyInformation?.logo
            Alamofire.request( image!).responseData { (response ) in
                
                self.CompanyLogo.image = UIImage(data : response.data!)
                
                
            }
            
            
        }
         
        
    }

    
    var followcompany = "2"
    
    @IBAction func follow(_ sender: UIButton) {
        
        
        let user_id = USERID.getUserID()
        if user_id == 0 {
            let alert = UIAlertController(title: "Alert", message: "you should loin first  ", preferredStyle: UIAlertControllerStyle.alert)
            
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                    self.navigationController?.pushViewController(linkingVC, animated: true)
                    
                    
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }}))
        }
        else{
            
            if followcompany == "2" {
                
                let info = [
                    "company_id"  : company_id ,
                    "type"  : 1
                ]
                Api.sharedInstance.follow(info: info) { (error, success, status) in
                    
                    if status == 1{
                        
                        sender.setTitle("متابع", for: .normal )
                        self.followcompany = "1"
                        self.getCompanyDetails()
                    }
                    else   if status == 0{
                        
                        sender.setTitle("تابع", for: .normal )
                        self.followcompany = "2"
                        self.getCompanyDetails()
                    }
                        
                    else
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: "you shoud loin first  ", preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                                self.navigationController?.pushViewController(linkingVC, animated: true)
                                
                                
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                                
                                
                            }}))
                        
                        alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }}))
                    }
                }
            }
                
            else if followcompany == "1" {
                let info = [
                    "company_id"  : company_id ,
                    "type"  : 2
                ]
                
                Api.sharedInstance.follow(info: info) { (error, success, status) in
                    
                    if status == 0{
                        
                        sender.setTitle("تابع", for: .normal )
                        self.followcompany = "2"
                    }
                    else  if status == 1{
                        
                        sender.setTitle("متابع", for: .normal )
                        self.followcompany = "1"
                    }
                    else
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: "you shoud loin first  ", preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let linkingVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController")
                                self.navigationController?.pushViewController(linkingVC, animated: true)
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }}))
                        alert.addAction(UIAlertAction(title: "cansel", style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                print("default")
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }}))
                        
                        
                    }
                    
                }
                
            }
        }
        
    }
}

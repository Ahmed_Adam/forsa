//
//  ForsaHomeCategoriesTableViewCell.swift
//  Forsa
//
//  Created by Adam on 1/23/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class ForsaHomeCategoriesTableViewCell: UITableViewCell  {



    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var disLikeButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyLogoButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CompanyForgetPasswordViewController.swift
//  Forsa
//
//  Created by Adam on 2/19/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class CompanyForgetPasswordViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    var info = [String:String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

 
    @IBAction func sendPassword(_ sender: Any) {
        
        info = ["phone": phone.text!]
        Api.sharedInstance.companyrResetPassword(info: info) { (error, success, response) in
            
        }
    }
    

}

//
//  SliderViewController.swift
//  Forsa
//
//  Created by Adam on 1/4/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import ImageSlideshow

class SliderViewController: UIViewController {

    @IBOutlet var sliderView: ImageSlideshow!
   var arrayImages = [ImageSource]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderView.backgroundColor = UIColor.white
        sliderView.slideshowInterval = 5.0
        sliderView.pageControlPosition = PageControlPosition.underScrollView
        sliderView.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        sliderView.pageControl.pageIndicatorTintColor = UIColor.black
        sliderView.contentScaleMode = UIViewContentMode.center
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        sliderView.activityIndicator = DefaultActivityIndicator()
        sliderView.currentPageChanged = { page in
            print("current page:", page)
        }
        
         self.arrayImages.append(ImageSource(image:#imageLiteral(resourceName: "Background")))
        self.arrayImages.append(ImageSource(image:#imageLiteral(resourceName: "Clothing_icon")))
        self.arrayImages.append(ImageSource(image:#imageLiteral(resourceName: "Market_icon")))
        sliderView.setImageInputs(arrayImages)
         self.sliderView.setImageInputs(self.arrayImages)
        
    }
    

    
    @objc func didTap() {
        let fullScreenController = sliderView.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }


}

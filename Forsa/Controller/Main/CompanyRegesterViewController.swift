//
//  CompanyRegesterViewController.swift
//  Forsa
//
//  Created by Adam on 2/4/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import DropDown


class CompanyRegesterViewController: UIViewController , UIImagePickerControllerDelegate ,
UINavigationControllerDelegate{
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mobilePhone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var ChooseCountry: UIButton!
    var countryArr = [Country]()
    var countryNames = [String]()
    var country_id = "1"
    var city_id = "1"
    @IBOutlet weak var chooseCity: UIButton!
    var cityArr = [City]()
    var cityNames = [String]()
    
    @IBAction func selectCountry(_ sender: UIButton) {
        
        
        let dd = DropDown()
        dd.anchorView = (sender as AnchorView)
        dd.dataSource = countryNames
        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
        dd.selectionAction = {[unowned self](index : Int , item : String)in
            sender.setTitle(item, for: .normal)
        }
        
        dd.show()
        
    }
    
    @IBAction func selectCity(_ sender: UIButton) {
        
        
        let dd = DropDown()
        dd.anchorView = (sender as AnchorView)
        dd.dataSource = cityNames
        dd.bottomOffset = CGPoint(x: 0, y:(dd.anchorView?.plainView.bounds.height)!)
        dd.selectionAction = {[unowned self](index : Int , item : String)in
            sender.setTitle(item, for: .normal)
        }
        dd.show()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        getCountries()
        getCities()
    }
    @IBOutlet weak var profilePhoto: UIImageView!
    
    @IBAction func selectProfilePhoto(_ sender: UIButton) {
        
        let myPicController = UIImagePickerController()
        myPicController.delegate = self
        myPicController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPicController , animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePhoto.contentMode = .scaleAspectFit
            profilePhoto.image = image
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func CompanyReg(_ sender: Any) {
        
        let name = self.name.text?.trimed
        let email = self.email.text?.trimed
        let password = self.password.text?.trimed
        let mobilePhone = self.mobilePhone.text?.trimed
        let photo = profilePhoto.image
        let address = self.address.text
        let info = ["name":name! , "email" : email! , "password" :password! , "mobilePhone":mobilePhone! , "country_id" : country_id , "city_id" : city_id , "address" : address]   as! [String : String]
        
        // check the empity fields
        if ((name?.isEmpty)! || (email?.isEmpty)! || (password?.isEmpty)! || (mobilePhone?.isEmpty)!){
            
            displayMessage(message: "All fields must be filled")
            return
        }
        else {
            
            Api.sharedInstance.companyRegister(info: info , image : photo , complition: { (error, sccess, status ) in
                
                if   status == 1{
                    
                    AccountType.saveAccountType(type: "company")
                    let home = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                    self.present(home!, animated: true, completion: nil)
                    
                }
                else if status == 0 {
                    
                    let alert = UIAlertController(title: "Alert", message: "Wrong Id or Password ", preferredStyle: UIAlertControllerStyle.alert)
                    
                    self.present(alert, animated: true, completion: nil)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            print("default")
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    
                }
                
            })
        }
        
    }
    
    func displayMessage(message : String){
        
        let myAllert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok",style: UIAlertActionStyle.default, handler :nil)
        myAllert.addAction(okAction)
        self.present(myAllert,animated :true , completion : nil)
        
        
    }
    
    
    func getCountries(){
        
        Api.sharedInstance.GetCountries { (error, success, countriesArr) in
            
            self.countryArr = countriesArr!
            for data in countriesArr! {
                self.countryNames.append(data.name)
            }
        }
        
    }
    
    func getCities(){
        
        Api.sharedInstance.GetCities(country_id: country_id) { (error, success, citiesArr) in
            self.cityArr = citiesArr!
            for data in citiesArr! {
                self.cityNames.append(data.name)
            }
        }
        
    }



}




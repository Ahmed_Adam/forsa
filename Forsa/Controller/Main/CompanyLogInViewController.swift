//
//  CompanyLogInViewController.swift
//  Forsa
//
//  Created by Adam on 2/17/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class CompanyLogInViewController: UIViewController {


    @IBOutlet weak var companyPasswordTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    @IBAction func CompanyLogInButton(_ sender: Any) {
        
        let info = ["username" : companyNameTextField.text  ,
                    "password" : companyPasswordTextField.text ]
        
        Api.sharedInstance.companyLogin(info: info as! [String : String]) { (error, sccess , status ) in
            
            if   status == true{
                AccountType.saveAccountType(type: "company")
                let home = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                self.present(home!, animated: true, completion: nil)
                
            }
            else if status == false {
                
                let alert = UIAlertController(title: "Alert", message: "Wrong Id or Password ", preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
                
            }
            
        }
        
        
    }



}

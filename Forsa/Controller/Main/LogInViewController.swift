//
//  LogInViewController.swift
//  Forsa
//
//  Created by Adam on 1/9/18.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit
import FacebookLogin


class LogInViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()

//       
//        fbLogin.addTarget(self, action: #selector(self.loginButtonClicked), for: .touchUpInside)

    }
    
    @IBAction func loginnwithoutRegister(_ sender: Any) {
        AccountType.saveAccountType(type: "")
        USERID.saveUserID(id: 0)
    }
    
    
    @objc func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: nil) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, _):
                print("Logged in!")
//                
//                if let accessToken = AccessToken.current {
//                    // User is logged in, use 'accessToken' here.
//                }
//                
                
                
                
            }
        }
    }
            
            
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    
    @IBOutlet weak var TwitterLogIn: UIButton!
    
    @IBOutlet weak var fbLogin: UIButton!
 


    
    
    @IBAction func Twitter(_ sender: Any) {
        
    }
    
    @IBAction func UserLogInButton(_ sender: Any) {
        
        let info = ["username" : userNameTextField.text  ,
                    "password" : userPasswordTextField.text ]
        
        Api.sharedInstance.login(info: info as! [String : String]) { (error, sccess , status ) in
            
            if   status == 1{
                
                AccountType.saveAccountType(type: "user")
                let home = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                self.present(home!, animated: true, completion: nil)
               
                }
             else if status == 0 {
                
                let alert = UIAlertController(title: "Alert", message: "Wrong Id or Password ", preferredStyle: UIAlertControllerStyle.alert)
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                        
                    }}))
                
            }
                
            }
            
            
        }
}
    

